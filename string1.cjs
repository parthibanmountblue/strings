function amount(testString) {
    let myNum = parseFloat(testString.replace("$", "").replace(",", ""));
    if (isNaN(myNum)) {
        return 0;
    }
    return myNum;
}

module.exports = amount;