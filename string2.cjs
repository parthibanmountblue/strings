function myArray(testString) {
    let arr = testString.split(".");
    for (ele in arr) {
        arr[ele] = parseInt(arr[ele]);
        if (isNaN(arr[ele])) {
            arr = [];
            break;
        }
    }
    return arr;
}
module.exports = myArray;